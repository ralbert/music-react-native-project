import axios from 'axios';
import _ from 'lodash';

const API_KEY = 'gGifpLDbv5mshD73h7dsFX02Ib37p1fWIIfjsnNPny90yCDkaZ';

const axiosInstance = axios.create({
    baseURL: 'https://deezerdevs-deezer.p.mashape.com/',
    timeout: 2000,
    headers: {'X-Mashape-Key': API_KEY}
});

export const searchTracks = singerName => { // no hacen falta los parentesis al rededor de singerName ya es es solo un atributo
    return axiosInstance.get(`search?q=${singerName}`).then (
        response => {
            const albums = response.data.data.map((item) => item.album);
            return _.uniqBy(albums, 'title'); // lodash permite extraer valores de un array omitiendo los valores repetidos utilizando una propiedad pasada por parametro
        })
}
/*
export const searchTracks = singerName => { // no hacen falta los parentesis al rededor de singerName ya es es solo un atributo
    return axiosInstance.get(`search?q=${singerName}`).then (
        (data) => {
            debugger;
            return data;
        })
}*/
/*
export const searchTracks = singerName => { // no hacen falta los parentesis al rededor de singerName ya es es solo un atributo
    return axiosInstance.get(`search?q=${singerName}`).then (
        (response) => {
            const albums = response.data.data.map((item) => item.album);
            return albums;
        })
}*/