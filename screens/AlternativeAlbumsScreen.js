import React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { Card, Text, Button } from 'react-native-elements';
import { CardList } from '../components/CardList';
import { SearchText } from '../components/SearchText';

import * as actions from '../actions';

export default class AlternativeAlbumsScreen extends React.Component {
    static navigationOptions = {
        title: 'Albums',
    };

    constructor() {
        super();

        this.state = {
            albums: [],  //si se coloca esto en null en el cardList habrá que crear una condicion para evitar que el map genere un error cuando se levata el componente
            isFeching: false // para colocar un valor por defecto antes de que se haaga una busqueda
        }    

        this.searchTracks = this.searchTracks.bind(this);  //duda con esto 
        /*
        actions.searchTracks('eminem').then(
            (albums) => {
                debugger;
            });*/       
    }
    searchTracks(artist) {//esta puede ser enviada por Props como se muestra ne la linea 34
        this.setState({isFeching: true, albums: []});
        actions.searchTracks(artist)
        .then(albums => this.setState({albums, isFeching: false}))
        .catch(err => this.setState({albums: [], isFeching: false}));  
    }

    renderAlbumView() {
        const { albums, isFeching } = this.state;   
        return (
            <ScrollView style={styles.container}>   
            <SearchText submitSearch={this.searchTracks}></SearchText> 

            { albums.length > 0 && !isFeching && //esto al aprecer es la respuesta al if
                <CardList   data={albums} 
                            imageKey={'cover_big'} 
                            titleKey={'title'}
                            buttonText={'See the detail'}>
                </CardList>
            }
            {
                albums.length === 0 && isFeching &&
                <Text>is loading ...</Text>
            }

            </ScrollView>
        )
    }
    //artist es obtenido y a la vez pasado a la función que a su vez será pasada a travez de props a un componente con el valor
    render() {  
        return this.renderAlbumView();
        /* 
        const { albums } = this.state;   
        return (
        <ScrollView style={styles.container}>   
            <SearchText submitSearch={this.searchTracks}></SearchText> 
            <CardList   data={albums} 
                        imageKey={'cover_big'} 
                        titleKey={'title'}
                        buttonText={'See the detail'}></CardList>
        </ScrollView>
        );*/
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});

/*
el codigo aqui fue movido al card list
    renderAlbums() {
        const { albums } = this.state;
        return albums.map((album, index) => {
            return (
                <Card
                    title={album.title}
                    image={{uri: album.image}}>           
                    <Button
                        icon={{name: 'code'}}
                        backgroundColor='#03A9F4'
                        buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
                        title='VIEW NOW' />
                </Card>
            )
        })
    }
*/