import React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { Card, Text, Button } from 'react-native-elements';

export default class AlbumsScreen extends React.Component {
    static navigationOptions = {
        title: 'Albums',
    };

    constructor() {
        super();

        this.state = {
                albums: [
                    {
                        title: 'Meteora',
                        image: 'https://upload.wikimedia.org/wikipedia/en/thumb/b/b2/Meteora_Live_Around.jpg/220px-Meteora_Live_Around.jpg'
                    },
                    {
                        title: 'Meteora',
                        image: 'https://upload.wikimedia.org/wikipedia/en/thumb/b/b2/Meteora_Live_Around.jpg/220px-Meteora_Live_Around.jpg'
                    },
                    {
                        title: 'Meteora',
                        image: 'https://upload.wikimedia.org/wikipedia/en/thumb/b/b2/Meteora_Live_Around.jpg/220px-Meteora_Live_Around.jpg'
                    },
                    {
                        title: 'Meteora',
                        image: 'https://upload.wikimedia.org/wikipedia/en/thumb/b/b2/Meteora_Live_Around.jpg/220px-Meteora_Live_Around.jpg'
                    }
                ]
        }

    }


    render() {
        const { albums } = this.state;

        return (
        <ScrollView style={styles.container}>         
            { albums.map((album, index) => {
                return (
                    <Card
                        title={album.title}
                        image={{uri: album.image}}>           
                        <Button
                            icon={{name: 'code'}}
                            backgroundColor='#03A9F4'
                            buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
                            title='VIEW NOW' />
                    </Card>
                )
            })
                
            }
        </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});