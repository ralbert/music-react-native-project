import React from 'react';
import { View } from 'react-native';
import { Card, Text, Button } from 'react-native-elements';

export class CardList extends React.Component {
  renderData() {
    const { data, imageKey, titleKey, buttonText} = this.props; //por que proviene de AlternativeAlbumScreen a travez de props
    return data.map((item, index) => {
        return (
            <Card
                key={index} //index es el numero de arriba y va a ser único en cada iteración
                title={item[titleKey]}//el paramerto titleKey viene con la palabra "title" lo que permite servir de parametro dentro de la lista de items
                image={{uri: item[imageKey]}}>           
                <Button
                    icon={{name: 'code'}}
                    backgroundColor='#03A9F4'
                    buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
                    title={ buttonText }/>
            </Card>
        )
    })
  }

  render() {
    const { data } = this.props;  //destructuring

    if ( data && data.length > 0 ){
      return this.renderData();
    } else {
      return <View></View>
    }
    
  }
}