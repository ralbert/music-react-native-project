import React from 'react';
import { Text, StyleSheet } from 'react-native';
import { FormLabel, FormInput, Button, FormValidationMessage} from 'react-native-elements'

// el react fragment solo se cre para tener un componente padre que encapsule a los hijos
export class SearchText extends React.Component {

    constructor() {
        super();

        this.state = {
            value: ''
        }
    }

    componentDidMount(){
        this.input.focus();
    }

    onChange(value) {
        this.setState({value});
    }

    onSubmitSearch() {
        const { submitSearch } = this.props;     
        submitSearch(this.state.value)
    }

        // el codigo despues del ref en el form input es copiado de rect bativ components input y ña funcion se llama "this.input.focus();"
    render() {
        return (
        <React.Fragment>  
            <FormLabel containerStyle={styles.center}>Search an artist</FormLabel>
            <FormInput ref={input => this.input = input} onChangeText={(event) => this.onChange(event) }/>
            <FormValidationMessage></FormValidationMessage>
            <Button title='Search' onPress={()=> this.onSubmitSearch() }/>
        </React.Fragment>
        )
    }
}

const styles = StyleSheet.create({
    center: {
        alignItems: 'center'
    }
})